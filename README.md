# Firmware para DSP F28335 de emulador eólico

## Preparación para utilizar el repositorio

### Requisitos

* Instalar Code Composer Studio versión 5.0+ incluyendo el soporte para procesadores C2000
* Instalar los drivers para los procesadores C2000 de acuerdo al [sitio de Texas Instruments](https://software-dl.ti.com/ccs/esd/documents/ccs_downloads.html)
* Instalar la suite [C2000Ware](https://www.ti.com/tool/C2000WARE)

### 1. Clonar el repositorio 

Clonar el repositorio utilizando el siguiente comando

```bash
git clone https://gitlab.com/jazzer11/emulador-eolico-firmware.git
```

### 2. Importar el proyecto en Code Composer Studio

Para esto, un a vez abierto la IDE de Code Composer Studio, ir a *Project -> Import CCS Projects...* o importar desde la extensi de *git* de CCS.

### 3. Selecciónar el entorno de compilación de acuerdo al sistema operativo

Establecer el entorno de compilación adecuado, según si se está trabajando en Linux o Windows. Ir a la configuración del proyecto y seleccionar la apropiada o hacer click derecho en el proyecto en la ventana del explorador de proyectos y seleccionar el entorno de compilación correcto en *Build Configurations -> Set Active*

**Nota**: Tener en cuenta que las variables de entorno apuntan a los archivos y bibliotecas de texas en su directorio de instalación por defecto
